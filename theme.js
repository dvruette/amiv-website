// eslint-disable-next-line import/no-extraneous-dependencies
import { vars as defaultVars } from 'polythene-style';

export const vars = Object.assign({}, defaultVars, {
  color_primary: '83, 120, 225', // new base color: #5378e1
});
