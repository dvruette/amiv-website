// Contains static information about our ressorts.
// Markdown can be used to style the text.

const data = [
  {
    name: 'Designteam',
    description: {
      en: 'Not a real description.',
      de: 'Keine echte Beschreibung',
    },
  },
  {
    name: 'ER Team',
    description: {
      en: 'Not a real description.',
      de: 'Keine echte Beschreibung',
    },
  },
  {
    name: 'IT Team',
    description: {
      en: 'Not a real description.',
      de: 'Keine echte Beschreibung',
    },
  },
  {
    name: 'Kulturteam',
    description: {
      en: 'Not a real description.',
      de: 'Keine echte Beschreibung',
    },
  },
];

export { data };
