// Contains static information about our commissions.
// Markdown can be used to style the text.

const data = [
  {
    name: 'Bastli',
    description: {
      de: `Das Elektroniklabor des AMIV von und für ETH Studenten bietet Euch kostenlose Arbeitsplätze und Werkzeuge um eure eigenen Projekte und Ideen umzusetzen.

          Wir sind für alle da und helfen euch gerne. Vorkentnisse werden keine benötigt, Hauptsache ihr habt Spass daran Dinge zu bauen ;)`,
    },
    website: 'https://bastli.ethz.ch',
    email: 'info@bastli.ethz.ch',
    phone: '+41 44 632 49 41',
  },
  {
    name: 'Blitz',
    website: 'https://blitz.ethz.ch',
    email: 'info@blitz.ethz.ch',
  },
  {
    name: 'Braukommission',
    description: {
      en: 'Not a real description.',
      de: 'Keine echte Beschreibung',
    },
  },
  {
    name: 'Irrational Co.',
    description: {
      en: 'Not a real description.',
      de: 'Keine echte Beschreibung',
    },
  },
  {
    name: 'EESTEC',
    description: {
      en: 'Not a real description.',
      de: 'Keine echte Beschreibung',
    },
  },
  {
    name: 'Funkbude',
    description: {
      en: 'Not a real description.',
      de: 'Keine echte Beschreibung',
    },
  },
  {
    name: 'LIMES - Ladies in Mechanical and Electrical Studies',
    description: {
      en: 'Not a real description.',
      de: 'Keine echte Beschreibung',
    },
    website: 'https://limes.amiv.ethz.ch',
    email: 'limes@amiv.ethz.ch',
  },
  {
    name: 'RandomDudes',
    description: {
      en: 'Not a real description.',
      de: 'Keine echte Beschreibung',
    },
  },
  {
    name: 'Kontakt',
    description: {
      en: 'Not a real description.',
      de: 'Keine echte Beschreibung',
    },
  },
  {
    name: 'E=MC^2',
    description: {
      en: 'Not a real description.',
      de: 'Keine echte Beschreibung',
    },
  },
  {
    name: 'MNS',
    description: {
      en: 'Not a real description.',
      de: 'Keine echte Beschreibung',
    },
  },
  {
    name: 'BEEZ',
    description: {
      en: 'Not a real description.',
      de: 'Keine echte Beschreibung',
    },
  },
];

export { data };
